import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-inspector-nav',
  templateUrl: './inspector-nav.component.html',
  styleUrls: ['./inspector-nav.component.scss']
})
export class InspectorNavComponent implements OnInit {
  userImage:any;
  getCookieValue = {};
  constructor(private router:Router) { }

  ngOnInit() {
    this.userImage=localStorage.getItem('userImg');
    this.getCookieValue=this.getlocalStorage('userData')['email'];
  }
  getlocalStorage(cookieName) {
    return JSON.parse(localStorage.getItem(cookieName));
}
logout(){
      /* Removing Cookies */
      localStorage.removeItem('userData');
      localStorage.removeItem('claimInsuranceVehicleNo');
      localStorage.removeItem('insurance_for');
      localStorage.removeItem('planId');  
      localStorage.removeItem('userImg');
      this.router.navigateByUrl('/');
  }
}
