import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectorNavComponent } from './inspector-nav.component';

describe('InspectorNavComponent', () => {
  let component: InspectorNavComponent;
  let fixture: ComponentFixture<InspectorNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InspectorNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectorNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
