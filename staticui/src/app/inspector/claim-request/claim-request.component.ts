import { Component, OnInit,TemplateRef,ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import swal from 'sweetalert';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
@Component({
  selector: 'app-claim-request',
  templateUrl: './claim-request.component.html',
  styleUrls: ['./claim-request.component.scss']
})
export class ClaimRequestComponent implements OnInit {
  claimRequestObj = [];
  rejectData:any=[];
  showTable: boolean = false;
  modalRef: BsModalRef;

  approvalObj = {
    approve: '',
    reason:''
    
};
@ViewChild('dummy') otpModal: TemplateRef<any>;
buttonClickFlag = '';
  constructor(
    private _http: HttpClient,
    private modalService: BsModalService,
  ) { }

  ngOnInit() {
    this.getClaimingRequest();
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
}
closeModal(){
    this.modalRef.hide();
}

getClaimingRequest() {
    var apiClaimRequest = 'https://indiuminsurance.ml/api/insurance-portal/inspector-get-claim-request';
    this._http.get(apiClaimRequest).subscribe(data => {
        // console.log('data', data);
        if (data['claimData'].length > 0) {
            this.showTable = true;
        }

        this.claimRequestObj = data['claimData'];
    }, error => {
        // console.log('error', error);
    })
}

approveClaimRequest(vehicleNo: any, approval: any) {
    
    var apiApprovals = 'https://indiuminsurance.ml/api/insurance-portal/inspector-approval?vehicleNo=' + vehicleNo;

    if (this.buttonClickFlag == 'yes') {
        this.approvalObj.approve = 'yes';
        swal({
            text:'Claim Request is approved',
            icon:'success'
        });
        
    } else {
        this.approvalObj.approve = 'no';
        this.approvalObj.reason=this.rejectData.reason;
        // console.log('reason',this.approvalObj.reason);
        swal({
            text:'Claim Request is rejected',
            icon:'error'
        })
        this.closeModal();
    }

    this._http.post(apiApprovals, this.approvalObj).subscribe(
        data => {
            // console.log('this.approvalObj', this.approvalObj);
            // console.log('data', data);
            this.getClaimingRequest();
        }, error => {
            // console.log('error', error);
        });
}

approveBtn() {
    this.buttonClickFlag = 'yes';
}
rejectBtn(){
   this.buttonClickFlag='no';
   this.openModal(this.otpModal)
   
  
}


}
