import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inspector-dashboard',
  templateUrl: './inspector-dashboard.component.html',
  styleUrls: ['./inspector-dashboard.component.scss']
})
export class InspectorDashboardComponent implements OnInit {

  userEmail = {};
  constructor() { }

  ngOnInit() {
    this.userEmail = this.getlocalStorage('userData')['email'];
  }
  getlocalStorage(cookieName) {
    return JSON.parse(localStorage.getItem(cookieName));
  }

}
