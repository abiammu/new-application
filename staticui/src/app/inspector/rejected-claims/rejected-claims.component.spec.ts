import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RejectedClaimsComponent } from './rejected-claims.component';

describe('RejectedClaimsComponent', () => {
  let component: RejectedClaimsComponent;
  let fixture: ComponentFixture<RejectedClaimsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RejectedClaimsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectedClaimsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
