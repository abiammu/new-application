import { Component, OnInit,TemplateRef, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import swal from 'sweetalert';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public signinObj: any = {};
  userObj = {};
  blockimage = '';
  modalRef: BsModalRef;
  public inputData: any = {};
  otpIsVerified: any;
  isVerified: any = '';
  verified: any;
  public loading: boolean;
  defaultImg: any;
  userImage: any;
  hideForm: boolean;
  successmsg: boolean;

  apiLogin = 'https://indiuminsurance.ml/api/insurance-portal/user-signin';


  @ViewChild('dummy') otpModal: TemplateRef<any>;

  constructor(private http:HttpClient,public modalService: BsModalService,private _router:Router) { }

  ngOnInit() {
    this.hideForm = true;
    this.successmsg = false;
    localStorage.removeItem('userData');
    localStorage.removeItem('claimInsuranceVehicleNo');
    localStorage.removeItem('insurance_for');
    localStorage.removeItem('planId');
    this.signinObj = {
      email: '',
      password: '',
      otp: '',
    }

    

    this.defaultImg = './assets/images/avatar1.png';
  }

  getlocalStorage(cookieName) {
    return JSON.parse(localStorage.getItem(cookieName));
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
 
  }
  closeModal() {
    if (typeof this.modalRef !== 'undefined') {
      this.modalRef.hide();
    }else{
      
    }
  }

  otpverification() {
    var apiOtp = 'https://indiuminsurance.ml/api/insurance-portal/verifyOtp?email=' + this.signinObj.email;
    this.http.post(apiOtp, this.signinObj).subscribe(
      data => {
        this.userSignin();
      },
      error => {
        if (error.status == 409) {
          swal({
            text: "Incorrect OTP",
            icon: "error",
          });
        }

      }
    );

  }
  isUserVerified() {
    var apiUserVerified = 'https://indiuminsurance.ml/api/insurance-portal/isUserVerified?email=' + this.signinObj.email;
    this.http.get(apiUserVerified).subscribe(
      data => {
        this.otpIsVerified = data;

        this.isVerified = this.otpIsVerified.data.isVerified;

        
      }
    )
  }


  upload() {
    var uploadimage = 'https://indiuminsurance.ml/api/insurance-portal/user-image?email=' + this.signinObj.email;

    this.http.get(uploadimage).subscribe(
      (data) => {
        //this.blockimage = data.image;
        //console.log("image",this.blockimage);
        this.userImage = data;
        this.userImage = 'https://indiuminsurance.ml/' + this.userImage.userData.image;
        this.userImage = this.userImage.replace(/\/public/, '');

        localStorage.setItem('userImg', this.userImage);
      }
    )
  }

  userSignin() {

    this.http.post(this.apiLogin, this.signinObj).subscribe(
      data => {
        this.hideForm = false;
        this.successmsg = true;
        this.isUserVerified();

        // res=>{
        // Save data in Session Storage
        this.userObj = {
          email: data['userData']['email'],
          role: data['userData']['role'],
          orgName: data['userData']['orgName']
        };
        localStorage.setItem('userData', JSON.stringify(this.userObj));

        if (data['userData']['role'] == 'insurance_holder') {
          location.href = '/holder-dashboard';
          swal({

            text: "Insurance Holder login successfully",
            icon: "success",
          });

          // this._router.navigateByUrl('/holder-dashboard');
          this.closeModal();
        } else if (data['userData']['role'] == 'insurance_provider') {
          location.href = '/provider-dashboard';
          swal({

            text: "Insurance Provider login successfully",
            icon: "success",
          });
          
          // this._router.navigateByUrl('/provider-dashboard');
          this.closeModal();
        } else if (data['userData']['role'] == 'inspector') {
          location.href = '/inspector-dashboard';
          swal({

            text: "Inspector login successfully",
            icon: "success",
          });
          // this._router.navigateByUrl('/inspector-dashboard');
          this.closeModal();


        }
      },
      error => {

        if (error.status == 402) {
          this.openModal(this.otpModal);
        }
        if (error.status == 404) {
          swal({

            text: "User not found check the mail address",
            icon: "error",
          });
        }
        if (error.status == 401) {
          swal({

            text: "Incorrect Password",
            icon: "error",
          });
        }

      }
    );
  }
  

}
