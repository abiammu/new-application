import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from './authguard/auth.guard'
import { HomeComponent } from './home/home.component';
import { PublicViewPlansComponent } from './home/public-view-plans/public-view-plans.component';
import { HolderDashboardComponent } from './insurance-holder/holder-dashboard/holder-dashboard.component';
import { ProviderDashboardComponent } from './insurance-provider/provider-dashboard/provider-dashboard.component';
import { InspectorDashboardComponent } from './inspector/inspector-dashboard/inspector-dashboard.component';
import { ShowPlanComponent } from './insurance-holder/show-plan/show-plan.component';
import { ViewPlanComponent } from './insurance-holder/view-plan/view-plan.component';
import { BuyPlansComponent } from './insurance-holder/buy-plans/buy-plans.component';
import { PoliciesComponent } from './insurance-holder/policies/policies.component';
import { VehicleToClaimComponent } from './insurance-holder/vehicle-to-claim/vehicle-to-claim.component';
import { ClaimReasonComponent } from './insurance-holder/claim-reason/claim-reason.component';
import { ClaimStatusComponent } from './insurance-holder/claim-status/claim-status.component';
import { ProviderShowPlansComponent } from './insurance-provider/provider-show-plans/provider-show-plans.component';
import { ProviderViewPlansComponent } from './insurance-provider/provider-view-plans/provider-view-plans.component';
import { AddPlansComponent } from './insurance-provider/add-plans/add-plans.component';
import { AddCategoryComponent } from './insurance-provider/add-category/add-category.component';
import { ProviderClaimstatusComponent } from './insurance-provider/provider-claimstatus/provider-claimstatus.component';
import { ClaimRequestComponent } from './inspector/claim-request/claim-request.component';
import { ApprovedClaimsComponent } from './inspector/approved-claims/approved-claims.component';
import { RejectedClaimsComponent } from './inspector/rejected-claims/rejected-claims.component';
import { BlockchainDashboardComponent } from './blockchain-dashboard/blockchain-dashboard.component';
import { BlockCountComponent } from './blockchain-dashboard/block-count/block-count.component';
import { BlockHashComponent } from './blockchain-dashboard/block-hash/block-hash.component';
import { BlockClaimComponent } from './blockchain-dashboard/block-claim/block-claim.component';
import { ProviderBlockclaimComponent } from './blockchain-dashboard/provider-blockclaim/provider-blockclaim.component';
import { InspectorClaimstatusComponent } from './blockchain-dashboard/inspector-claimstatus/inspector-claimstatus.component';
import { HashfileComponent } from './insurance-holder/hashfile/hashfile.component';



const routes: Routes = [
{path:'',component:HomeComponent},
{path:'public-view-plans',component:PublicViewPlansComponent},
/*Insurance-Holder */
{path:'holder-dashboard',component:HolderDashboardComponent,canActivate: [AuthGuard],
data: { 
  expectedRole: 'insurance_holder'}},
{path:'show-plan',component:ShowPlanComponent,canActivate: [AuthGuard],
data: { 
  expectedRole: 'insurance_holder'}},
{path:'view-plan',component:ViewPlanComponent,canActivate: [AuthGuard],
data: { 
  expectedRole: 'insurance_holder'}},
{path:'buy-plans',component:BuyPlansComponent,canActivate: [AuthGuard],
data: { 
  expectedRole: 'insurance_holder'}},
{path:'policies',component:PoliciesComponent,canActivate: [AuthGuard],
data: { 
  expectedRole: 'insurance_holder'}},
{path:'vehicle-to-claim',component:VehicleToClaimComponent,canActivate: [AuthGuard],
data: { 
  expectedRole: 'insurance_holder'}},
{path:'claim-reason',component:ClaimReasonComponent,canActivate: [AuthGuard],
data: { 
  expectedRole: 'insurance_holder'}},
{path:'claim-status',component:ClaimStatusComponent,canActivate: [AuthGuard],
data: { 
  expectedRole: 'insurance_holder'}},
{path:'claim-insurance',component:VehicleToClaimComponent,canActivate: [AuthGuard],
data: { 
  expectedRole: 'insurance_holder'}},
  {path:'hashfile',component:HashfileComponent,canActivate: [AuthGuard],
data: { 
  expectedRole: 'insurance_holder'}},

{path:'blockchain-dashboard',component:BlockchainDashboardComponent},




// {path:'block-count',component:BlockCountComponent, canActivate: [AuthGuard],
// data: { 
//   expectedRole: 'inspector'
// }},
{path:'block-count',component:BlockCountComponent},
{path:'block-hash',component:BlockHashComponent},
{path:'block-claim',component:BlockClaimComponent},
{path:'provider-blockclaim',component:ProviderBlockclaimComponent},
  {path:'inspector-claimstatus',component:InspectorClaimstatusComponent},
// {path:'block-count',component:BlockCountComponent, canActivate: [AuthGuard],
// data: { 
//   expectedRole: 'insurance_provider'
// }},
// {path:'block-count',component:BlockCountComponent, canActivate: [AuthGuard],
// data: { 
//   expectedRole: 'insurance_holder'
// }},

// {path:'block-hash',component:BlockHashComponent, canActivate: [AuthGuard],
// data: { 
//   expectedRole: 'inspector'
// }},
// {path:'block-hash',component:BlockHashComponent, canActivate: [AuthGuard],
// data: { 
//   expectedRole: 'insurance_provider'
// }},
// {path:'block-hash',component:BlockHashComponent, canActivate: [AuthGuard],
// data: { 
//   expectedRole: 'insurance_holder'
// }},

// {path:'block-claim',component:BlockClaimComponent, canActivate: [AuthGuard],
// data: { 
//   expectedRole: 'inspector'  
// }},
// {path:'block-claim',component:BlockClaimComponent, canActivate: [AuthGuard],
// data: { 
//   expectedRole: 'insurance_provider' 
// }},
// {path:'block-claim',component:BlockClaimComponent, canActivate: [AuthGuard],
// data: { 
//   expectedRole: 'insurance_holder'
// }},

/* Insurance-Provider */
{path:'provider-dashboard',component:ProviderDashboardComponent,canActivate: [AuthGuard],
data: { 
  expectedRole: 'insurance_provider'}},
{path:'provider-show-plans',component:ProviderShowPlansComponent,canActivate: [AuthGuard],
data: { 
  expectedRole: 'insurance_provider'}},
{path:'provider-view-plans',component:ProviderViewPlansComponent,canActivate: [AuthGuard],
data: { 
  expectedRole: 'insurance_provider'}},
{path:'add-plans',component:AddPlansComponent,canActivate: [AuthGuard],
data: { 
  expectedRole: 'insurance_provider'}},
{path:'add-category',component:AddCategoryComponent,canActivate: [AuthGuard],
data: { 
  expectedRole: 'insurance_provider'}},
{path:'provider-claimstatus',component:ProviderClaimstatusComponent,canActivate: [AuthGuard],
data: { 
  expectedRole: 'insurance_provider'}},
/* Inspector */
{path:'inspector-dashboard',component:InspectorDashboardComponent,canActivate: [AuthGuard],
data: { 
  expectedRole: 'inspector'}},
{path:'claim-request',component:ClaimRequestComponent,canActivate: [AuthGuard],
data: { 
  expectedRole: 'inspector'}},
{path:'approved-claims',component:ApprovedClaimsComponent,canActivate: [AuthGuard],
data: { 
  expectedRole: 'inspector'}},
{path:'rejected-claims',component:RejectedClaimsComponent,canActivate: [AuthGuard],
data: { 
  expectedRole: 'inspector'}},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
