import { BrowserModule } from '@angular/platform-browser';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { TooltipModule, TooltipConfig } from 'ngx-bootstrap';
import { NgxPasswordToggleModule } from 'ngx-password-toggle';
import {
  AppAsideModule,
} from '@coreui/angular';
import {HttpClientModule} from '@angular/common/http';
import { NgModule, ElementRef } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import {FormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './authguard/auth.guard';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './user/login/login.component';
import { RegisterComponent } from './user/register/register.component';
import { ShowPlansComponent } from './home/show-plans/show-plans.component';
import { PublicViewPlansComponent } from './home/public-view-plans/public-view-plans.component';
import {AuthService} from './auth.service';
import { PublicNavbarComponent } from './home/public-navbar/public-navbar.component';
import {AppHeaderModule} from '@coreui/angular';
import { ModalModule, BsModalService } from 'ngx-bootstrap';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { BsDropdownModule, BsDropdownDirective } from 'ngx-bootstrap/dropdown';
import { HolderDashboardComponent } from './insurance-holder/holder-dashboard/holder-dashboard.component';
import { ProviderDashboardComponent } from './insurance-provider/provider-dashboard/provider-dashboard.component';
import { InspectorDashboardComponent } from './inspector/inspector-dashboard/inspector-dashboard.component';
import { HolderNavComponent } from './insurance-holder/holder-nav/holder-nav.component';
import { ShowPlanComponent } from './insurance-holder/show-plan/show-plan.component';
import { ViewPlanComponent } from './insurance-holder/view-plan/view-plan.component';
import { HolderHomeComponent } from './insurance-holder/holder-home/holder-home.component';
import { BuyPlansComponent } from './insurance-holder/buy-plans/buy-plans.component';
import { PoliciesComponent } from './insurance-holder/policies/policies.component';
import { VehicleToClaimComponent } from './insurance-holder/vehicle-to-claim/vehicle-to-claim.component';
import { ClaimReasonComponent } from './insurance-holder/claim-reason/claim-reason.component';
import { ClaimStatusComponent } from './insurance-holder/claim-status/claim-status.component';
import { ProviderNavComponent } from './insurance-provider/provider-nav/provider-nav.component';
import { AddPlansComponent } from './insurance-provider/add-plans/add-plans.component';
import { AddCategoryComponent } from './insurance-provider/add-category/add-category.component';
import { ProviderClaimstatusComponent } from './insurance-provider/provider-claimstatus/provider-claimstatus.component';
import { ProviderViewPlansComponent } from './insurance-provider/provider-view-plans/provider-view-plans.component';
import { ProviderShowPlansComponent } from './insurance-provider/provider-show-plans/provider-show-plans.component';
import { ProviderHomeComponent } from './insurance-provider/provider-home/provider-home.component';
import { ClaimRequestComponent } from './inspector/claim-request/claim-request.component';
import { ApprovedClaimsComponent } from './inspector/approved-claims/approved-claims.component';
import { RejectedClaimsComponent } from './inspector/rejected-claims/rejected-claims.component';
import { InspectorNavComponent } from './inspector/inspector-nav/inspector-nav.component';
import { BlockchainDashboardComponent } from './blockchain-dashboard/blockchain-dashboard.component';
import { BlockClaimComponent } from './blockchain-dashboard/block-claim/block-claim.component';
import { BlockHashComponent } from './blockchain-dashboard/block-hash/block-hash.component';
import { BlockCountComponent } from './blockchain-dashboard/block-count/block-count.component';
import { BlockDataComponent } from './blockchain-dashboard/block-data/block-data.component';
import { ProviderBlockclaimComponent } from './blockchain-dashboard/provider-blockclaim/provider-blockclaim.component';
import { InspectorClaimstatusComponent } from './blockchain-dashboard/inspector-claimstatus/inspector-claimstatus.component';
import {HashfileComponent} from './insurance-holder/hashfile/hashfile.component'

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    ShowPlansComponent,
    PublicViewPlansComponent,
    PublicNavbarComponent,
    HolderDashboardComponent,
    ProviderDashboardComponent,
    InspectorDashboardComponent,
    HolderNavComponent,
    ShowPlanComponent,
    ViewPlanComponent,
    HolderHomeComponent,
    BuyPlansComponent,
    PoliciesComponent,
    VehicleToClaimComponent,
    ClaimReasonComponent,
    ClaimStatusComponent,
    ProviderNavComponent,
    AddPlansComponent,
    AddCategoryComponent,
    ProviderClaimstatusComponent,
    ProviderViewPlansComponent,
    ProviderShowPlansComponent,
    ProviderHomeComponent,
    ClaimRequestComponent,
    ApprovedClaimsComponent,
    RejectedClaimsComponent,
    InspectorNavComponent,
    BlockchainDashboardComponent,
    BlockClaimComponent,
    BlockHashComponent,
    BlockCountComponent,
    BlockDataComponent,
    ProviderBlockclaimComponent,
    InspectorClaimstatusComponent,
    HashfileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AppAsideModule,
    AppHeaderModule,
    PerfectScrollbarModule,
    ReactiveFormsModule,
    FormsModule,
    TooltipModule,
    NgxPasswordToggleModule,
    BsDropdownModule.forRoot(),
    NgbModule.forRoot(),
    ModalModule,
    HttpClientModule,
  ],
  providers: [AuthService,BsModalService,TooltipConfig,AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
