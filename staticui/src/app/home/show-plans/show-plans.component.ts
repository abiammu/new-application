import { Component, OnInit,TemplateRef,ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
@Component({
  selector: 'app-show-plans',
  templateUrl: './show-plans.component.html',
  styleUrls: ['./show-plans.component.scss']
})
export class ShowPlansComponent implements OnInit {
  modalRef: BsModalRef;

  @ViewChild('plans') optModal: TemplateRef<any>;

  constructor(public router:Router,public modalService: BsModalService) { }

  ngOnInit() {
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    // console.log('this.modalRef:', typeof this.modalRef.hide)
  }
  viewinsurance(insurance_for) {
    localStorage.setItem('insurance_for', insurance_for);

    this.router.navigate(['/public-view-plans']);
  }


}
