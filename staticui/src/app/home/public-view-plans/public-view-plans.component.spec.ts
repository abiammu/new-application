import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicViewPlansComponent } from './public-view-plans.component';

describe('PublicViewPlansComponent', () => {
  let component: PublicViewPlansComponent;
  let fixture: ComponentFixture<PublicViewPlansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicViewPlansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicViewPlansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
