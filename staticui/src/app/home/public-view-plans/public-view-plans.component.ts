import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth.service';
import swal from 'sweetalert';
import {Insurance} from 'src/app/Insurance';

@Component({
  selector: 'app-public-view-plans',
  templateUrl: './public-view-plans.component.html',
  styleUrls: ['./public-view-plans.component.scss']
})
export class PublicViewPlansComponent implements OnInit {
  public insurances: Insurance[];
  showDataFlag:boolean=false;
  constructor(public router:Router,public _auth:AuthService) { }

  ngOnInit() {
      this.viewinsurance();
  }
  buy(plansId: any) {
    localStorage.setItem('planId', plansId);
    swal({

        text: "Please Login To Buy Insurance",
        icon: "warning",
    });
}
back(){
    this.router.navigate([''])
}


viewinsurance() {
    var getInsuranceFor = localStorage.getItem('insurance_for')
    this._auth.viewinsurance(getInsuranceFor).subscribe((data: Insurance[]) => {
        // console.log('plan data:', data);
        if(data['payload'].length > 0){
            this.showDataFlag = true;
        }
        this.insurances = data;
        // console.log(this.insurances);
    });
}

 
}
