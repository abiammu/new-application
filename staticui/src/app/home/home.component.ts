import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public isView:boolean;

  constructor() { }

  ngOnInit() {
   this.isView=true;

  }
  public toggle(): void { 
    this.isView = !this.isView; 
  }
}



