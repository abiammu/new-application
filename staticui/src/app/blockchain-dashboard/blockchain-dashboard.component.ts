import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-blockchain-dashboard',
  templateUrl: './blockchain-dashboard.component.html',
  styleUrls: ['./blockchain-dashboard.component.scss']
})
export class BlockchainDashboardComponent implements OnInit {
  getUserData = {};
  constructor() { }

  ngOnInit() {
    this.getUserData = this.getlocalStorage('userData').role;
        // console.log('getUserData', this.getUserData);
  }
  getlocalStorage(cookieName) {
    return JSON.parse(localStorage.getItem(cookieName));
 }

}
