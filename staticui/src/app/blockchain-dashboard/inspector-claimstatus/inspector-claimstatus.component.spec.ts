import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectorClaimstatusComponent } from './inspector-claimstatus.component';

describe('InspectorClaimstatusComponent', () => {
  let component: InspectorClaimstatusComponent;
  let fixture: ComponentFixture<InspectorClaimstatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InspectorClaimstatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectorClaimstatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
