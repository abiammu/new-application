import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockClaimComponent } from './block-claim.component';

describe('BlockClaimComponent', () => {
  let component: BlockClaimComponent;
  let fixture: ComponentFixture<BlockClaimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlockClaimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockClaimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
