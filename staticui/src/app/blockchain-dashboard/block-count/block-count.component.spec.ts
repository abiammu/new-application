import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockCountComponent } from './block-count.component';

describe('BlockCountComponent', () => {
  let component: BlockCountComponent;
  let fixture: ComponentFixture<BlockCountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlockCountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
