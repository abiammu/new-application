import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockHashComponent } from './block-hash.component';

describe('BlockHashComponent', () => {
  let component: BlockHashComponent;
  let fixture: ComponentFixture<BlockHashComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlockHashComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockHashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
