import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-block-hash',
  templateUrl: './block-hash.component.html',
  styleUrls: ['./block-hash.component.scss']
})
export class BlockHashComponent implements OnInit {
  blockDetailsObj = [];
  constructor(private _http:HttpClient,private router:Router) { }

  ngOnInit() {
    this.getblockdetails();  
  }
  getblockdetails() {
    var apigetBlockDetails = 'https://indiuminsurance.ml/api/insurance-portal/get-block-details';
    this._http.get(apigetBlockDetails).subscribe(data => {
        this.blockDetailsObj = data['blockData'];
    }, error => {
        // console.log('error', error);
    })
    }

}
