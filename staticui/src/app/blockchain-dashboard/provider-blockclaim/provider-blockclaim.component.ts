import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-provider-blockclaim',
  templateUrl: './provider-blockclaim.component.html',
  styleUrls: ['./provider-blockclaim.component.scss']
})
export class ProviderBlockclaimComponent implements OnInit {
getCookieValue={};
allResults: any=[];
datas: any = [];
showDataFlag:boolean=false;
  constructor(private _http:HttpClient) { }

  ngOnInit() {
    this.getCookieValue = this.getlocalStorage('userData').email;
    console.log('getUserData', this.getCookieValue);
    this.getclaimdetails();

  }
  getlocalStorage(cookieName) {
    return JSON.parse(localStorage.getItem(cookieName));
 }
getclaimdetails(){
  var apigetBlockvehicleDetails = 'https://indiuminsurance.ml/api/insurance-portal/user-claimsBC?email=' + this.getCookieValue;

    this._http.get(apigetBlockvehicleDetails).subscribe((data:any) => {
      // console.log('data',data);
      for (var i = 0; i < data.payload.length; i++) {
        this.datas = data.payload[i].Record;
        if(data.payload.length > 0){
          this.showDataFlag = true;
      }
        // console.log("data",this.datas)
         this.allResults.push(this.datas)
      }

      // console.log(this.allResults);
      error=>{
        // console.log('error',error)
      }
    });
}
}
