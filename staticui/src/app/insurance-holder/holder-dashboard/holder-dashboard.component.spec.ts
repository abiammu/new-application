import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HolderDashboardComponent } from './holder-dashboard.component';

describe('HolderDashboardComponent', () => {
  let component: HolderDashboardComponent;
  let fixture: ComponentFixture<HolderDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HolderDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HolderDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
