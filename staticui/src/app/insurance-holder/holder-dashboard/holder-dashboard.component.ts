import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-holder-dashboard',
  templateUrl: './holder-dashboard.component.html',
  styleUrls: ['./holder-dashboard.component.scss']
})
export class HolderDashboardComponent implements OnInit {
  userEmail = {};
  constructor() { }

  ngOnInit() {
    this.userEmail = this.getlocalStorage('userData')['email'];
  }
  getlocalStorage(cookieName) {
    return JSON.parse(localStorage.getItem(cookieName));
  }
}
