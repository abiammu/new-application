import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HolderNavComponent } from './holder-nav.component';

describe('HolderNavComponent', () => {
  let component: HolderNavComponent;
  let fixture: ComponentFixture<HolderNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HolderNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HolderNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
