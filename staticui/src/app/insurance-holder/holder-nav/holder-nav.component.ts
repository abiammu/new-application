import { Component, OnInit } from '@angular/core';
import { navigationItems } from './nav';
import { Router } from '@angular/router';

@Component({
  selector: 'app-holder-nav',
  templateUrl: './holder-nav.component.html',
  styleUrls: ['./holder-nav.component.scss']
})
export class HolderNavComponent implements OnInit {
  userImage:any;
  getCookieValue = {};
  public navItems = [];
  constructor(private router:Router) {
    {
      this.navItems = navigationItems;
    }
   }

  ngOnInit() {
    this.userImage=localStorage.getItem('userImg');
    this.getCookieValue=this.getlocalStorage('userData')['email'];
  }
  getlocalStorage(cookieName) {
    return JSON.parse(localStorage.getItem(cookieName));
}
// openNav() {
//   document.getElementById("mySidebar").style.display = "block";
// }
// closeNav() {
//   document.getElementById("mySidebar").style.display = "none";
// }
logout(){
  /* Removing Cookies */
  localStorage.removeItem('userData');
  localStorage.removeItem('claimInsuranceVehicleNo');
  localStorage.removeItem('insurance_for');
  localStorage.removeItem('planId');  
  localStorage.removeItem('userImg');
  localStorage.removeItem('filehash');
  this.router.navigateByUrl('/');
}

}
