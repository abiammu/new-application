import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth.service';
import { Insurance } from 'src/app/Insurance';
import { Router } from '@angular/router';
@Component({
  selector: 'app-view-plan',
  templateUrl: './view-plan.component.html',
  styleUrls: ['./view-plan.component.scss']
})
export class ViewPlanComponent implements OnInit {
  public insurances: Insurance[];
  getUserData = {};
  showDataFlag: boolean = false;
  constructor(private _auth:AuthService,private router:Router) { }

  ngOnInit() {
    this.viewinsurance();
    this.getUserData = this.getlocalStorage('userData').role;
  }
  back(){
    this.router.navigate(['/show-plan'])
  }
  getlocalStorage(cookieName) {
    return JSON.parse(localStorage.getItem(cookieName));
}

buy(plansId:any) {
    localStorage.setItem('planId', plansId); 
    // console.log(plansId.insurance_provider_name);
    this.router.navigate(['/buy-plans']);
}
  viewinsurance() {
    var getInsuranceFor = localStorage.getItem('insurance_for')
    this._auth.viewinsurance(getInsuranceFor).subscribe((data: Insurance[]) => {
        // console.log('plan data:', data);
        if(data['payload'].length > 0){
            this.showDataFlag = true;
        }

        this.insurances = data;
        // console.log(this.insurances);
    });
}
}
