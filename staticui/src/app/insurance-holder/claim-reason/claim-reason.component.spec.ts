import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClaimReasonComponent } from './claim-reason.component';

describe('ClaimReasonComponent', () => {
  let component: ClaimReasonComponent;
  let fixture: ComponentFixture<ClaimReasonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClaimReasonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClaimReasonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
