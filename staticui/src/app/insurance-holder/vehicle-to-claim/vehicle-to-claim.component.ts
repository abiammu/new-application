import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-vehicle-to-claim',
  templateUrl: './vehicle-to-claim.component.html',
  styleUrls: ['./vehicle-to-claim.component.scss']
})
export class VehicleToClaimComponent implements OnInit {
  vehicleData = [];
  claimStatusData=[];

  constructor(private _http: HttpClient) { }

  ngOnInit() {
    this.getClaimVehicleData();
  }

  getClaimVehicleData() {
    var getVehicleNo = localStorage.getItem('claimInsuranceVehicleNo');
  
    var apiGetClaimVehicle = 'https://indiuminsurance.ml/api/insurance-portal/user-claim-insurance-vehicle-data?vehicleNo=' + getVehicleNo


    this._http.get(apiGetClaimVehicle).subscribe(
        data => {
            this.vehicleData = data['vehicle_data'];
            // console.log('this.vehicleData', this.vehicleData);
        },
        error => {
            // console.log('error', error);
        }
    );
    }


}




