import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleToClaimComponent } from './vehicle-to-claim.component';

describe('VehicleToClaimComponent', () => {
  let component: VehicleToClaimComponent;
  let fixture: ComponentFixture<VehicleToClaimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleToClaimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleToClaimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
