import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HashfileComponent } from './hashfile.component';

describe('HashfileComponent', () => {
  let component: HashfileComponent;
  let fixture: ComponentFixture<HashfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HashfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HashfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
