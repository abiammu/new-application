import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth.service';
import { Router } from '@angular/router';
import swal from 'sweetalert';
import { Insurance } from 'src/app/Insurance';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-policies',
  templateUrl: './policies.component.html',
  styleUrls: ['./policies.component.scss']
})
export class PoliciesComponent implements OnInit {
  public insurances: Insurance[];
  getEmail = {};
  insuredObj = {};
  vehicleNo='';
  claimStatusData=[];
  showDataFlag:boolean=false;
  constructor(
    private _http: HttpClient,
    private _auth: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getEmail = this.getlocalStorage('userData');
    this.insurances = [];
    this.getpolicies();
    this.vehicleNo;
  }

  getpolicies() {
    this._auth
      .getpolicies(this.getEmail['email'])
      .subscribe((data: Insurance[]) => {
        // console.log('insured data:', data);
        
        if(data['insuredData'].length > 0){
            this.showDataFlag = true
        }

        this.insurances = data;
        // console.log(this.insurances);
      });
  }

  getlocalStorage(cookieName) {
    return JSON.parse(localStorage.getItem(cookieName));
  }
  
  claim(vehicleNo: any) {
    localStorage.setItem('claimInsuranceVehicleNo',vehicleNo );
    // this.vehicleNo=localStorage.setItem('claimInsuranceVehicleNo',vehicleNo );
    var apiGetClaimVehicle = 'https://indiuminsurance.ml/api/insurance-portal/user-claim-insurance-data?vehicleNo=' + vehicleNo


    this._http.get(apiGetClaimVehicle).subscribe(
        data => {
          
            this.claimStatusData = data['claim_data'];
            // console.log('this.vehicleData', this.claimStatusData);
            if(this.claimStatusData==vehicleNo){
              // console.log(vehicleNo)
              swal({
                text:'Claim Already Initiated',
                icon:'error'
              })

            }else if(data['message']=="Vehicle Number Not Found!"){
              
             
            }
        },
        error => {
            
            this.router.navigate(['/claim-insurance']);

        }
    );
   
   // 
  }

  
}



