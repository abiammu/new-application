import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HolderHomeComponent } from './holder-home.component';

describe('HolderHomeComponent', () => {
  let component: HolderHomeComponent;
  let fixture: ComponentFixture<HolderHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HolderHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HolderHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
