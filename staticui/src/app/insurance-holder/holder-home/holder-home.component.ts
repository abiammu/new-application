import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-holder-home',
  templateUrl: './holder-home.component.html',
  styleUrls: ['./holder-home.component.scss']
})
export class HolderHomeComponent implements OnInit {
  userImage: any;
  getCookieValue = {};
  constructor(private router: Router) { }

  ngOnInit() {
    this.userImage = localStorage.getItem('userImg');
    this.getCookieValue = this.getlocalStorage('userData')['email'];
  }

  getlocalStorage(cookieName) {
    return JSON.parse(localStorage.getItem(cookieName));
  }

  back() {
    this.router.navigate(['/show-plan'])
  }
  logout(){
    /* Removing Cookies */
    localStorage.removeItem('userData');
    localStorage.removeItem('claimInsuranceVehicleNo');
    localStorage.removeItem('insurance_for');
    localStorage.removeItem('planId');  
    localStorage.removeItem('userImg');
    this.router.navigateByUrl('/');
  }
  
}
