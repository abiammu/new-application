import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderShowPlansComponent } from './provider-show-plans.component';

describe('ProviderShowPlansComponent', () => {
  let component: ProviderShowPlansComponent;
  let fixture: ComponentFixture<ProviderShowPlansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderShowPlansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderShowPlansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
