import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-provider-show-plans',
  templateUrl: './provider-show-plans.component.html',
  styleUrls: ['./provider-show-plans.component.scss']
})
export class ProviderShowPlansComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  viewinsurance(insurance_for) {
    localStorage.setItem('insurance_for', insurance_for);
    this.router.navigate(['/provider-view-plans']);
  }

}
