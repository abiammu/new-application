import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-provider-home',
  templateUrl: './provider-home.component.html',
  styleUrls: ['./provider-home.component.scss']
})
export class ProviderHomeComponent implements OnInit {
  userImage: any;
  getCookieValue = {};
  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    this.userImage = localStorage.getItem('userImg');
    this.getCookieValue = this.getlocalStorage('userData')['email'];
  }
  getlocalStorage(cookieName) {
    return JSON.parse(localStorage.getItem(cookieName));
  }

  back() {
    this.router.navigate(['/provider-show-plans'])
  }

}
