import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderClaimstatusComponent } from './provider-claimstatus.component';

describe('ProviderClaimstatusComponent', () => {
  let component: ProviderClaimstatusComponent;
  let fixture: ComponentFixture<ProviderClaimstatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderClaimstatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderClaimstatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
