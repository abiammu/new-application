import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderViewPlansComponent } from './provider-view-plans.component';

describe('ProviderViewPlansComponent', () => {
  let component: ProviderViewPlansComponent;
  let fixture: ComponentFixture<ProviderViewPlansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderViewPlansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderViewPlansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
