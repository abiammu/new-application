import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth.service';
import { Insurance } from 'src/app/Insurance';
import { Router } from '@angular/router';
@Component({
  selector: 'app-provider-view-plans',
  templateUrl: './provider-view-plans.component.html',
  styleUrls: ['./provider-view-plans.component.scss']
})
export class ProviderViewPlansComponent implements OnInit {
  public insurances: Insurance[];
  getUserData = {};
  getemail={};
  showDataFlag: boolean = false;
  constructor(private _auth: AuthService, private router: Router) { }

  ngOnInit() {
    this.viewinsurerPlans();  
  }
  getlocalStorage(cookieName) {
    return JSON.parse(localStorage.getItem(cookieName));
}


viewinsurerPlans() {
    var getInsuranceFor = localStorage.getItem('insurance_for');
    /* getting user data from cookie */
    this.getUserData = this.getlocalStorage('userData').role;
    this.getemail=this.getlocalStorage('userData').email;
    
    this._auth.viewinsurerPlans(this.getemail,getInsuranceFor).subscribe((res: Insurance[]) => {
        if(res['payload'].length > 0){
            this.showDataFlag = true;
        }

        this.insurances = res;
        
    });
}


}
