import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-provider-nav',
  templateUrl: './provider-nav.component.html',
  styleUrls: ['./provider-nav.component.scss']
})
export class ProviderNavComponent implements OnInit {
  userImage:any;
  getCookieValue = {};
  constructor(private router:Router) { }

  ngOnInit() {
    this.userImage=localStorage.getItem('userImg');
    this.getCookieValue=this.getlocalStorage('userData')['email'];
  }
  getlocalStorage(cookieName) {
    return JSON.parse(localStorage.getItem(cookieName));
}
logout(){
  /* Removing Cookies */
  localStorage.removeItem('userData');
  localStorage.removeItem('claimInsuranceVehicleNo');
  localStorage.removeItem('insurance_for');
  localStorage.removeItem('planId');  
  localStorage.removeItem('userImg');
  this.router.navigateByUrl('/');
}

}
