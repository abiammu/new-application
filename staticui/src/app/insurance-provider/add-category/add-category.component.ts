import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth.service';
import swal from 'sweetalert';
@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent implements OnInit {
  InsurerData: any = {};
  getCookieValue = {};
  getorgName={};
  constructor(
    private router: Router, 
    private _auth: AuthService
  ) { }

  ngOnInit() {
    this.getCookieValue = this.getlocalStorage('userData')['email'];
    
    this.InsurerData.insurance_provider_name = this.getlocalStorage('userData')['orgName'];
  }
  getlocalStorage(cookieName) {
    return JSON.parse(localStorage.getItem(cookieName));
}
  insurercategory() {
    this.InsurerData.insurance_provider_name = this.getlocalStorage('userData')['orgName'];
    this._auth.insurercategory(this.InsurerData,this.getCookieValue).subscribe(
      res => {
        swal({
              
          text: "Category Added Successfully",
          icon: "success",
        });
        
      },
      err => {
        if (err.error.message == "category Already Added !" && err.error.status == 409) {
          swal({
              
            text: "Category Already Exists",
            icon: "error",
          });
        }
        if (err.error.message == " failed to get Insurance Category" && err.error.status == 500) {
          swal({
              
            text: "Failed to Get Insurance Category",
            icon: "error",
          });
        }
        
      }
      )      
  }
}



