var mongoose = require('mongoose');
var schema = mongoose.Schema;

var insurerAddCategorySchema = new schema({
    _id: mongoose.Schema.Types.ObjectId,
    insurance_provider_name: {
        type: String,
        required: true
    },
    category: {
        type: String,
        required: true
    },
    email:{
        type:String,
        required:true
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('InsurerAddCategory', insurerAddCategorySchema);