var mongoose = require('mongoose');
var schema = mongoose.Schema;

var claimingStatus = new schema({
    _id: mongoose.Schema.Types.ObjectId,
    email:{
        type: String,
        required: true
    },
    vehicleNo: {
        type: String,
        required: true
    },
    reason_for_rejection:{
        type: String,
    },
    claim_reason:{
        type: String,
        require: true
    },

    Incident_Date:{
        type:String,
        require: true
    },
    claim_reason_image:{
        type: String,
        require: true
    },
    claim_initiated: {
        type: String,
        default: 'no'
    },
    claim_status: {
        type: String,
        default: null
    },
    user_profile: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'UserInsure'
    },
    provider_name: {
        type: String,
        require: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Claiming-status', claimingStatus);