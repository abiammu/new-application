var mongoose = require('mongoose');
var schema = mongoose.Schema;

var userInsure = new schema({
    _id: mongoose.Schema.Types.ObjectId,
    email: {
        type: String,
        required: true
    },
    owner_name: {
        type: String,
        required: true
    },
    vehicle_model: {
        type: String,
        required: true
    },
    vehicle_number: {
        type: String,
        required: true
    },    
    vehicle_price: {
        type: String,
        required: true
    },
    purchased_date: {
        type: String,
        required: true
    },
    file_hash: {
        type: String,
        required: true
    },
    accident_involved: {
        type: String,
        required: true
    },
    comments: {
        type: String
    },    
    insurance_plans: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'InsurerAddPlans'
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('UserInsure', userInsure);