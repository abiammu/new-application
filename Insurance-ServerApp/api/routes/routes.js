'use strict';
const multer = require('multer');

module.exports = function (app) {
    // Multer Middleware: Image upload path
    const storage = multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, 'public/images/claim-reason-img/');
        },
        filename: (req, file, cb) => {
            cb(null, file.originalname);
        }
    });
    var upload = multer({ storage: storage });

    const storages = multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, 'public/user/');
        },
        filename: (req, file, cb) => {
            cb(null, file.originalname);
        }
    });
    var image = multer({ storage: storages });
    // Multer

    // only AddUser is connected with blockchain network
    //var addUser = require('../controllers/home/register');
    //app.route('/api/insurance-portal/register/user').post(addUser.addUser);
    /* User Image */
    var userImage = require('../controllers/signin/userImage');
    app.route('/api/insurance-portal/user-image').get(userImage.userImage);

    /* User Signin */
    var userSignin = require('../controllers/signin/signin');
    app.route('/api/insurance-portal/user-signin').post(userSignin.userSignin);
    //verification for otp
    var verifyOtp = require('../controllers/Email/verifyOtp');
    app.route('/api/insurance-portal/verifyOtp').post(verifyOtp.verifyotp);

    var isUserVerified = require('../controllers/Email/isUserVerified');
    app.route('/api/insurance-portal/isUserVerified').get(isUserVerified.isUserVerified);

    /* Insurance Provider */
    var insurerSignup = require('../controllers/insurer/insurer-signup');
    app.route('/api/insurance-portal/register-insurance-provider').post(image.single('image'),
        insurerSignup.insurerSignup
    );

    var insurerAddCategory = require('../controllers/insurer/add-category');
    app.route('/api/insurance-portal/insurer-addCategory').post(
        insurerAddCategory.insurerAddCategory
    );
    app.route('/api/insurance-portal/insurer-getCategory').get(insurerAddCategory.getCategory);

    var insurerAddPlans = require('../controllers/insurer/insurer-add-plans');
    app.route('/api/insurance-portal/insurer-addPlans').post(
        insurerAddPlans.insurerAddplans
    );

    var insurergetPlans = require('../controllers/user/get-plans');
    app.route('/api/insurance-portal/insurer-getplans').get(insurergetPlans.insurergetPlans);
    /* Insurance Holder */

    var userSignup = require('../controllers/user/policy-holder-signup');
    app.route('/api/insurance-portal/policy-holder-signup').post(image.single('image'),userSignup.policyHolderSignup);

    var usergetPlans = require('../controllers/user/get-plans');
    app.route('/api/insurance-portal/getplans').get(usergetPlans.getPlans);

    var userInsure = require('../controllers/user/insure');
    app.route('/api/insurance-portal/user-insure').post(
        userInsure.userApplyInsurance
    );


    var userGetInsuredData = require('../controllers/user/getInsuredData');
    app.route('/api/insurance-portal/user-getInsured-data').get(
        userGetInsuredData.getInsuredItem
    );

    var userClaimInsurance = require('../controllers/user/claim-insurance-vehicle');
    app.route('/api/insurance-portal/user-claim-insurance-vehicle-data').get(
        userClaimInsurance.getClaimInsuranceVehicleData
    );
    app.route('/api/insurance-portal/user-claim-insurance-data').get(
        userClaimInsurance.getClaimInsurancedata
    );


    app.route('/api/insurance-portal/user-claim-insurance').post(
        upload.single('claim_reason_image'), userClaimInsurance.claimInsurance
    );

    app.route('/api/insurance-portal/user-claim-status').get(
        userClaimInsurance.getClaimStatus
    );
    app.route('/api/insurance-portal/user-claim-statusBC').get(
        userClaimInsurance.getClaimStatusBC
    );
    var userClaims = require('../controllers/insurer/claims')
    app.route('/api/insurance-portal/user-claims').get(
        userClaims.getClaims
    );
    app.route('/api/insurance-portal/user-claimsBC').get(userClaims.getClaimsBC);

var fileHash = require('../controllers/user/getAllhash');
app.route('/api/insurance-portal/filehash').get(fileHash.getAllfiles);

    /* Inspector */
    var inspectorSignup = require('../controllers/inspector/inspector-signup');
    app.route('/api/insurance-portal/inspector-signup').post(image.single('image'),inspectorSignup.inspectorSignup);

    var inspectorViewClaimRequest = require('../controllers/inspector/getClaimRequest');
    app.route('/api/insurance-portal/inspector-get-claim-request').get(inspectorViewClaimRequest.getClaimInsuranceVehicleData);
    app.route('/api/insurance-portal/inspector-get-approved-claims').get(inspectorViewClaimRequest.getAllApprovedClaims)
    app.route('/api/insurance-portal/inspector-get-rejected-claims').get(inspectorViewClaimRequest.getAllRejectedClaims)
    var inspectorApproveClaim = require('../controllers/inspector/approve-request');
    app.route('/api/insurance-portal/inspector-approval').post(inspectorApproveClaim.approveClaims);

    var getallclaims = require('../controllers/inspector/getClaimsBC');
    app.route('/api/insurance-portal/inspector-claimsBC').get(getallclaims.getAllUserClaims);

    /* Blockchain APIs */

    var getuser = require('../BlockchainApp/user-certificates/user');
    app.route('/api/insurance-portal/get-user').get(getuser.getUser);

    var getalluservehicle = require('../BlockchainApp/get-vehicle');
    app.route('/api/insurance-portal/get-user-vehicle').get(getalluservehicle.getAllUserVehicle);
    var getallusers = require('../BlockchainApp/get-data');
    app.route('/api/insurance-portal/get-block-data').get(getallusers.getAllData);
    var getTotalblock = require('../BlockchainApp/get-total-blocks/getTotalBlocks');
    app.route('/api/insurance-portal/get-total-blocks').get(getTotalblock.getTotalAvailableBlocks);
    var getblockdetails = require('../BlockchainApp/get-block-details/getBlockDetails');
    app.route('/api/insurance-portal/get-block-details').get(getblockdetails.getBlockDetails);
    var getCurrenthash = require('../BlockchainApp/get-current-hash/getCurrentHash');
    app.route('/api/insurance-portal/get-block-hash').get(getCurrenthash.getCurrentAndPreviousHash);
};
