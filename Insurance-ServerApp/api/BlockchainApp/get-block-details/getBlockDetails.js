var Fabric_Client = require('fabric-client');
var path = require('path');
var os = require('os');

var blockLength = '';
var blockObj = {};
var blockArray = [];
var DEBUG = 1;

// Console.log function
function consolelog(message, variable) {
   if (DEBUG == 1) console.log(message, variable);
}

module.exports = (function() {
   return {
      getBlockDetails: function(req, res) {
         var fabric_client = new Fabric_Client();

         // setup the fabric network
         var channel = fabric_client.newChannel('mychannel');
         var peer = fabric_client.newPeer('grpc://localhost:7051');
         channel.addPeer(peer);

         //
         var member_user = null;
         var store_path = path.join(os.homedir(), '.hfc-key-store');
         console.log('Store path:' + store_path);
         var tx_id = null;

         // create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
         Fabric_Client.newDefaultKeyValueStore({
            path: store_path
         })
            .then(state_store => {
               // assign the store to the fabric client
               fabric_client.setStateStore(state_store);
               var crypto_suite = Fabric_Client.newCryptoSuite();
               // use the same location for the state store (where the users' certificate are kept)
               // and the crypto store (where the users' keys are kept)
               var crypto_store = Fabric_Client.newCryptoKeyStore({
                  path: store_path
               });
               crypto_suite.setCryptoKeyStore(crypto_store);
               fabric_client.setCryptoSuite(crypto_suite);

               // get the enrolled user from persistence, this user will sign all requests
               return fabric_client.getUserContext('saji', true);
            })
            .then(user_from_store => {
               if (user_from_store && user_from_store.isEnrolled()) {
                  console.log('Successfully loaded saji from persistence');
                  member_user = user_from_store;
               } else {
                  throw new Error(
                     'Failed to get saji.... run registerUser.js'
                  );
               }
            })
            .then(query_responses => {
               console.log('Getting Block Details');

               // Function to read Previous Hash
               async function asyncQueryBlockByNumber() {
                  var blockInfo = await channel.queryInfo();
                  //Get Block Length
                  blockLength = JSON.parse(blockInfo.height);
                  console.log('Block Length:', blockLength);

                  blockObj = {};
                  blockArray = [];

                  for (i = 0; i < blockLength; i++) {
                     //channel.queryBlock(1) helps query block by number
                     var result = await channel.queryBlock(i);
                     blockObj = {
                        blockNumber: result.header.number,
                        previous_hash: result.header.previous_hash,
                        data_hash: result.header.data_hash,
                     };
                     blockArray.push(blockObj);
                  }

                  return blockArray;
               }

               asyncQueryBlockByNumber()
                  .then(() => {
                     res.status(200).json({
                        blockData: blockArray
                     });
                  })
                  .catch(err => {
                     console.error('Failed to get block data :: ' + err);
                  });

               /* Getting block details ends */
            })
            .catch(err => {
               console.error('Failed to find block details :: ' + err);
            });
      }
   };
})();
