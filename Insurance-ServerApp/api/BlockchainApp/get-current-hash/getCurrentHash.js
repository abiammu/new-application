var Fabric_Client = require('fabric-client');
var path = require('path');
var os = require('os');
var sha = require('js-sha256');
var asn = require('asn1.js');

var blockLength = '';
var blockObj = {};
var blockArray = [];
var currentHash = [];
var DEBUG = 1;

// Console.log function
function consolelog(message, variable) {
   if (DEBUG == 1) console.log(message, variable);
}

module.exports = (function() {
   return {
      getCurrentAndPreviousHash: function(req, res) {
         var fabric_client = new Fabric_Client();

         // setup the fabric network
         var channel = fabric_client.newChannel('mychannel');
         var peer = fabric_client.newPeer('grpc://localhost:7051');
         channel.addPeer(peer);

         //
         var member_user = null;
         var store_path = path.join(os.homedir(), '.hfc-key-store');
         console.log('Store path:' + store_path);
         var tx_id = null;

         // create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
         Fabric_Client.newDefaultKeyValueStore({
            path: store_path
         })
            .then(state_store => {
               // assign the store to the fabric client
               fabric_client.setStateStore(state_store);
               var crypto_suite = Fabric_Client.newCryptoSuite();
               // use the same location for the state store (where the users' certificate are kept)
               // and the crypto store (where the users' keys are kept)
               var crypto_store = Fabric_Client.newCryptoKeyStore({
                  path: store_path
               });
               crypto_suite.setCryptoKeyStore(crypto_store);
               fabric_client.setCryptoSuite(crypto_suite);

               // get the enrolled user from persistence, this user will sign all requests
               return fabric_client.getUserContext('saji', true);
            })
            .then(user_from_store => {
               if (user_from_store && user_from_store.isEnrolled()) {
                  console.log('Successfully loaded saji from persistence');
                  member_user = user_from_store;
               } else {
                  throw new Error(
                     'Failed to get saji.... run registerUser.js'
                  );
               }
            })
            .then(query_responses => {
               console.log('Getting Current & Previous Hash');

               /* Start: Calculate Current Hash */

               var calculateBlockHash = function(header) {
                  let headerAsn = asn.define('headerAsn', function() {
                     this.seq().obj(
                        this.key('Number').int(),
                        this.key('PreviousHash').octstr(),
                        this.key('DataHash').octstr()
                     );
                  });

                  let output = headerAsn.encode(
                     {
                        Number: parseInt(header.number),
                        PreviousHash: Buffer.from(header.previous_hash, 'hex'),
                        DataHash: Buffer.from(header.data_hash, 'hex')
                     },
                     'der'
                  );
                  let hash = sha.sha256(output);
                  console.log('HASH..', hash);
                  currentHash.push(hash);
                  console.log('Current Hash', currentHash);
                  return hash;
               };

               /* End: Calculate Current Hash */

               // Start: Function to read Previous Hash
               async function asyncCurrentAndPreviousHash() {
                  var blockInfo = await channel.queryInfo();
                  //Get Block Length
                  blockLength = JSON.parse(blockInfo.height);
                  //console.log('Block Length:', blockLength);

                  for (i = 0; i < blockLength; i++) {
                     //channel.queryBlock(1) helps query block by number
                     var result = await channel.queryBlock(i);
                     blockObj = {
                        header: {
                           number: result.header.number,
                           previous_hash: result.header.previous_hash,
                           data_hash: result.header.data_hash,
                           current_hash: ''
                        }
                     };
                     blockArray.push(blockObj);
                     //console.log('Getting blockArray:',blockArray);
                     calculateBlockHash(blockArray[i].header);
                     //console.log('Current Hash Index:',currentHash[i]);
                     blockArray[i].header.current_hash = currentHash[i];
                     console.log( blockArray[i].header.current_hash)
                     //console.log('Getting blockArray:',blockArray);
                  }
                  // End: Function to read Previous Hash
               }

               asyncCurrentAndPreviousHash()
                  .then(() => {
                     res.status(200).json({
                        blockData: blockArray
                     });

                     blockArray=[];
                  })
                  .catch(err => {
                     console.error(
                        'Failed to get current & previous hash :: ' + err
                     );
                  });

               /* Getting block details ends */
            })
            .catch(err => {
               console.error('Failed to get current & previous hash :: ' + err);
            });
      }
   };
})();