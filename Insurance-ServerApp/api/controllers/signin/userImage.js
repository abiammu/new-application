
var signup = require('../../models/signup');

exports.userImage = function(req,res){
    var email= req.query.email.toLowerCase();

    signup.findOne({email:email},'image').exec().then(data=>{
        if(data){
            console.log( data)
            res.status(200).json({
                userData: data,
                status: 201
              });
        }
        else{
            res.status(404).json({
                message :"Emailid is not found",
                //email: data.email,
                status: 404
              });
        }

    }).catch(err=>{
        console.log(err.message);
        res.status(401).json({
            status: 401,
            error : err.message
          });
    })
}