const officegen = require('officegen')
const fs = require('fs');
const os= require('os');
var docx_pdf = require('docx-pdf');
const ipfsAPI = require('ipfs-api');

//Connceting to the ipfs network via infura gateway
const ipfs = ipfsAPI('ipfs.infura.io', '5001', {protocol: 'https'})
module.exports = function document(data){
  var file ='';

console.log(data)
// Create an empty Word object:
let docx = officegen('docx')

// Officegen calling this function after finishing to generate the docx document:
docx.on('finalize', function(written) {
  console.log(
    'Finish to create a Microsoft Word document.'+written
  )
})

// Officegen calling this function to report errors:
docx.on('error', function(err) {
  console.log(err)
})

// Create a new paragraph:
let pObj = docx.createP({align: 'center'})


var email = data.email;
var owner_name = data.owner_name;
var vehicle_model = data.vehicle_model;
var vehicle_number = data.vehicle_number;
var vehicle_price = data.vehicle_price;
var purchased_date = data.purchased_date;
var accident_involved = data.accident_involved;
var insurance_for = '2 wheelers',
insurance_provider_name= 'ICICI',
insurance_plan_year= '2 years',
yearly_premium= '2400',
about_insurance_plan= '2 Wheelers Plan for 2 years';

pObj.addText("Insurance Policy Declaration", { font_face: 'Liberation Serif', font_size: 23 })
pObj.addLineBreak ();

pObj = docx.createP()

pObj.addText('User Details',{ bold: true,font_face: 'Liberation Serif', font_size: 17})
pObj.addLineBreak ();
pObj.addLineBreak ();

pObj.addText('User Name :   ',{color:'333131',font_face: 'Liberation Serif', font_size: 12 ,bold: true})
pObj.addText(owner_name,{font_face: 'Liberation Serif', font_size: 12});
pObj.addLineBreak ();
pObj.addText('Useremail :   ',{color:'333131',font_face: 'Liberation Serif', font_size: 12,bold: true})
pObj.addText(email,{font_face: 'Liberation Serif', font_size: 12});
pObj.addLineBreak ();
pObj.addLineBreak ();

pObj.addText('__________________________________________________________________________')


pObj.addText('Vehicle Details ',{ bold: true,font_face: 'Liberation Serif', font_size: 17,background_color:'333131' })
pObj.addLineBreak ();
pObj.addLineBreak ();

pObj.addText('vehicle_model  :   ',{color:'333131',font_face: 'Liberation Serif', font_size: 12,bold: true})
pObj.addText(vehicle_model,{font_face: 'Liberation Serif', font_size: 12});
pObj.addLineBreak ();
pObj.addText('vehicle_number :   ',{color:'333131',font_face: 'Liberation Serif', font_size: 12,bold: true})
pObj.addText(vehicle_number,{font_face: 'Liberation Serif', font_size: 12});
pObj.addLineBreak ();
pObj.addText('vehicle_price  :   ',{color:'333131',font_face: 'Liberation Serif', font_size: 12,bold: true});
pObj.addText(vehicle_price,{font_face: 'Liberation Serif', font_size: 12});
pObj.addLineBreak ();

pObj.addText('purchased_date :   ',{color:'333131',font_face: 'Liberation Serif', font_size: 12,bold: true});
pObj.addText(purchased_date ,{font_face: 'Liberation Serif', font_size: 12});
pObj.addLineBreak ();

pObj.addText('accident_involved :   ',{color:'333131',font_face: 'Liberation Serif', font_size: 12,bold: true});
pObj.addText(accident_involved  ,{font_face: 'Liberation Serif', font_size: 12});
pObj.addLineBreak ();

pObj.addLineBreak ();


pObj.addText('____________________________________________________________________________');

pObj.addText('Policy Information ',{ bold: true,font_face: 'Liberation Serif', font_size: 17,background_color:'333131' })
pObj.addLineBreak ();
pObj.addLineBreak ();

pObj.addText('insurance_for  :   ',{color:'333131',font_face: 'Liberation Serif', font_size: 12,bold: true})
pObj.addText(insurance_for,{font_face: 'Liberation Serif', font_size: 12});
pObj.addLineBreak ();
pObj.addText('insurance_provider_name :   ',{color:'333131',font_face: 'Liberation Serif', font_size: 12,bold: true})
pObj.addText(insurance_provider_name,{font_face: 'Liberation Serif', font_size: 12});
pObj.addLineBreak ();
pObj.addText('insurance_plan_year  :   ',{color:'333131',font_face: 'Liberation Serif', font_size: 12,bold: true});
pObj.addText(insurance_plan_year,{font_face: 'Liberation Serif', font_size: 12});
pObj.addLineBreak ();

pObj.addText('yearly_premium :   ',{color:'333131',font_face: 'Liberation Serif', font_size: 12,bold: true});
pObj.addText(yearly_premium ,{font_face: 'Liberation Serif', font_size: 12});
pObj.addLineBreak ();

pObj.addText('about_insurance_plan :   ',{color:'333131',font_face: 'Liberation Serif', font_size: 12,bold: true});
pObj.addText(about_insurance_plan  ,{font_face: 'Liberation Serif', font_size: 12});
pObj.addLineBreak ();
docx.putPageBreak()


let out = fs.createWriteStream(email+'.docx')


out.on('error', function(err) {
  console.log(err)
})

// Async call to generate the output file:
docx.generate(out)

console.log(" hello")
  // docx_pdf(os.homedir()+"/Iswariya/new-application/Insurance-ServerApp/file.docx",'./output.pdf',function(err,result){
  //   if(err){
  //     console.log(err);
  //   }
  //   console.log('result'+result);
  
  // });
  var filepath =  os.homedir()+"/new-application/Insurance-ServerApp/"+email+".docx";
 
  return filepath;

  

//  setTimeout(()=>{
//    console.log("ccccccccccccccccccccccccccccccc",file)
//   return "hello "+file;
//  },2400)

}
