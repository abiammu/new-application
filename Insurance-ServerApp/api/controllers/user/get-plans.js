const mongoose = require('mongoose');
const plansSchema = mongoose.model('InsurerAddPlans');

var InsurerAddCategorySchema = require('../../models/insurer-add-category');

var InsurerSchema = require('../../models/insurer-signup');
/* exports.getPlans = function(req,res,next){
    plansSchema.find()
    .populate({path: 'insurance_category'})
    .exec()
    .then(data =>{
        console.log('object', data);
        res.status(200).json({
            message:"all insurance plans",
            payload:data
        })
    })
    .catch();
} */

exports.insurergetPlans = function (req, res, next) {
    InsurerSchema.findOne({ email: req.query.email }).exec().then((data) => {
        var provider_name = data.orgName;
        var vehicle = req.query.insurance_for.toLowerCase();
        InsurerAddCategorySchema.find({ insurance_provider_name: provider_name }).exec().then((data) => {
            var id = data[0]._id;
            console.log(id + data[0].category)
            plansSchema.find({ insurance_for: vehicle, insurance_category: id })
                .populate({ path: 'insurance_category' })
                .exec()
                .then(data => {
                    console.log('object', data);
                    res.status(200).json({
                        message: "all insurance plans",
                        payload: data
                    })
                })
                .catch(err => {
                    if (err) throw err;
                    res.json({
                        message: err
                    })
                });
        })
    }).catch(err => {
        if (err) throw err;
        res.json({
            message: err
        })
    });
}

exports.getPlans = function (req, res, next) {
    var vehicle = req.query.insurance_for.toLowerCase();

    plansSchema.find({ insurance_for: vehicle })
        .populate({ path: "insurance_category" })
        .exec()
        .then(data => {
            // if(data.insurance_category.insurance_provider_name==provider_name){
            console.log('object', data);
            res.status(200).json({
                message: "all insurance plans",
                payload: data
            })
            //  }
        })
        .catch();
} 