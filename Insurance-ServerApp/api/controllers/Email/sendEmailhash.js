var nodemailer = require('nodemailer');

exports.sendHash = function (email,hash) {

    var smtpTransport = nodemailer.createTransport({
        service: "Gmail",
        auth: {
            user: "insuranceindium@gmail.com",
            pass: "indiumsoft"
        }
    });
    var mail = {
        from: "indium insurance <insuranceindium@gmail.com>",
        to: email,
        subject: "Welcome to IndiumInsurance",
        text: "IPFS HASH FOR POLICY DOCUMENT.",
        html: "<b> Your IPFS Hash "+hash+ "</b>"+"</br>"+"Your link https://ipfs.io/ipfs/"+hash+"</br>"
    }

    smtpTransport.sendMail(mail, function (error, res) {
        if (error) {
            console.log(error);
        } else {
            console.log("Message sent: " + mail.text);
        }
        smtpTransport.close();
    });
} 
