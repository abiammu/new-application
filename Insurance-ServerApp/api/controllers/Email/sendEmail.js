var nodemailer = require('nodemailer');
var otp_generator = require('otp-generator');
var verifyschema = require('../../models/verification');
var LocalStorage = require('node-localstorage').LocalStorage;

exports.sendMail = function (email) {
    var otp = otp_generator.generate(5, { digits: true, alphabets: false, upperCase: false, specialChars: false });
    var verifySchema = new verifyschema({
        emailId: email,
        isVerified: false
    })
    if (typeof localStorage === "undefined" || localStorage === null) {
        localStorage = new LocalStorage('./local');
    }
    localStorage.setItem(email, otp);
    var smtpTransport = nodemailer.createTransport({
        service: "Gmail",
        auth: {
            user: "insuranceindium@gmail.com",
            pass: "indiumsoft"
        }
    });
    var mail = {
        from: "indium insurance <insuranceindium@gmail.com>",
        to: email,
        subject: "Welcome to IndiumInsurance",
        text: "Thanks for registering.",
        html: "<b>Thank You for registering with IndiumInsurance    Your OneTimePassword <br/>" + otp + "</b>"
    }

    smtpTransport.sendMail(mail, function (error, res) {
        if (error) {
            console.log(error);
        } else {
            console.log("Message sent: " + mail.text);
        }
        smtpTransport.close();
    });
    verifySchema.save().then(data => {
        console.log("saved  successfully");
    })
} 
